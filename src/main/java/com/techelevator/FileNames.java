package com.techelevator;

import java.io.File;

public class FileNames {
    public static File dex = new File("dex.txt");
    public static File elements = new File("elements.txt");
    public static File pokemon = new File("pokemon.txt");
    public static File region = new File("region.txt");

    public static File select(String userInput) {
        switch (userInput) {
            case "dex":
            return dex;

            case "elements":
            return elements;

            case "pokemon":
            return pokemon;

            case "region":
            return region;

            default:
            {System.out.println("Invalid selection. Now quitting.");
                return null;}
        }
    }
}
