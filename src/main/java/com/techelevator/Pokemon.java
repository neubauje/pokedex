package com.techelevator;

public class Pokemon {
    public int nationalDexNumber;
    public String species;
    public Boolean canEvolve;
    public Boolean hasEvolved;
    Element[] pokemonElement;
    public String color;
    public String shape;
    public Boolean canMega;
    public Boolean canGigantamax;
    public String eggGroup;

    public Pokemon(int nationalDexNumber,
                   String species,
                   Boolean canEvolve,
                   Boolean hasEvolved,
                   Element pokemonElement1,
                   Element pokemonElement2,
                   String color,
                   String shape,
                   Boolean canMega,
                   Boolean canGigantamax,
                   String eggGroup) {
        this.nationalDexNumber = nationalDexNumber;
        this.species = species;
        this.canEvolve = canEvolve;
        this.hasEvolved = hasEvolved;
        this.pokemonElement = new Element[2];
        this.pokemonElement[0] = pokemonElement1;
        this.pokemonElement[1] = pokemonElement2;
        this.color = color;
        this.shape = shape;
        this.canMega = canMega;
        this.canGigantamax = canGigantamax;
        this.eggGroup = eggGroup;
    }

}
