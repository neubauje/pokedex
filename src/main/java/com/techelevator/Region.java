package com.techelevator;

public class Region {
    private String regionName;
    private Boolean hasAlternateForms;

    public Region() {
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public Boolean getHasAlternateForms() {
        return hasAlternateForms;
    }

    public void setHasAlternateForms(Boolean hasAlternateForms) {
        this.hasAlternateForms = hasAlternateForms;
    }
}
