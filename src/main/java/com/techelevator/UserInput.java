package com.techelevator;

import java.util.Scanner;

public class UserInput {
    private static Scanner input = new Scanner(System.in);

    public static String prompt() {
        return input.nextLine().toLowerCase().trim();
    }

}
