package com.techelevator;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FileReader {


    public static List<String> readFile(File selection) throws FileNotFoundException {
        Scanner reader = new Scanner(selection);
        List<String> fileLines = new ArrayList<>();
        while (reader.hasNext()) {
            fileLines.add(reader.nextLine());
        }
        reader.close();
        return fileLines;
    }
}
