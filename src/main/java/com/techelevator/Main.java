package com.techelevator;

import java.io.FileNotFoundException;
import java.util.*;

public class Main {
    private static List<Element> allElements = new ArrayList<>();

    public static void main(String[] args) throws FileNotFoundException {
        readElementsFile();
        Random random = new Random();
        String selection = "";
        while (!selection.equals("quit")) {
            System.out.print("Select a pokedex mode- build, fill, analyze, or quit: ");
            selection = UserInput.prompt();
            if (selection.equals("build")){
                buildMenu();
            }
            else if (selection.equals("fill")){
                fillMenu();
            }
            else if (selection.equals("analyze")){
                analyzeMenu();
            }
            else if (selection.equals("quit")){System.out.println("Now quitting.");}
            else {System.out.println("That's not one of the options.");}
        }

    }

    private static void analyzeMenu() {
    }

    private static void fillMenu() {
    }

    private static void buildMenu() {
        String subselection = "";
        System.out.println("What would you like to build? Options: dex, types, pokemon, region");
        subselection = UserInput.prompt();
    }


    private static void readElementsFile() throws FileNotFoundException {
        List<String> fileLines = FileReader.readFile(FileNames.select("elements"));
        for (String fileLine: fileLines) {
            List<String> lineParts = new ArrayList<String>(Arrays.asList(fileLine.split(", ")));
            String name = lineParts.get(0);
            List<String> strengths = new ArrayList<>();
            List<String> weaknesses = new ArrayList<>();
            List<String> noEffect = new ArrayList<>();
            for (int i = 1; i < lineParts.size(); i++) {
                if (lineParts.get(i).startsWith("+")){
                    strengths.add(lineParts.get(i).substring(1));
                }
                if (lineParts.get(i).startsWith("-")){
                    weaknesses.add(lineParts.get(i).substring(1));
                }
                if (lineParts.get(i).startsWith("0")){
                    noEffect.add(lineParts.get(i).substring(1));
                }
            }
            if (noEffect.size() == 0) {
                allElements.add(new Element(name, strengths, weaknesses));}
            else {
                allElements.add(new Element(name, strengths, weaknesses, noEffect));}
        }

    }

}
