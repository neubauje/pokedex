package com.techelevator;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Element {
    private final String elementName;
    List<String> StrongAgainst = new ArrayList<String>();
    List<String> WeakAgainst = new ArrayList<String>();
    List<String> DoesNotEffect = new ArrayList<String>();
    private String notes;

    public Element(String elementName, List<String> strongAgainst, List<String> weakAgainst, List<String> doesNotEffect) {
        this.elementName = elementName;
        StrongAgainst = strongAgainst;
        WeakAgainst = weakAgainst;
        DoesNotEffect = doesNotEffect;
    }

    public Element(String elementName, List<String> strongAgainst, List<String> weakAgainst) {
        this.elementName = elementName;
        StrongAgainst = strongAgainst;
        WeakAgainst = weakAgainst;
        DoesNotEffect = null;
    }



    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }


}

