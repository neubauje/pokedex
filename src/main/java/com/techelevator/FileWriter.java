package com.techelevator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;

public class FileWriter {

    public static void writeFile(File selection, List<String> fileLines) throws FileNotFoundException {
        PrintWriter writer = new PrintWriter(selection);
        for (String line: fileLines) {
            writer.println(line);
        }
        writer.close();
    }
}
