package com.techelevator;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Dex {
    public static List<Pokemon> pokedex = new ArrayList<>();

    public static void buildDex() throws FileNotFoundException {
        List<String> fileLines = FileReader.readFile(FileNames.select("dex"));
        for (String fileLine: fileLines) {
            List<String> lineParts = new ArrayList<String>(Arrays.asList(fileLine.split(", ")));
            int nationalDexNumber = Integer.parseInt(lineParts.get(0));
            String species = lineParts.get(1);
            Boolean canEvolve = Boolean.valueOf(lineParts.get(2));
            Boolean hasEvolved = Boolean.valueOf(lineParts.get(3));
            String pokemonType1 = lineParts.get(4);
            String pokemonType2 = lineParts.get(5);
            String color = lineParts.get(6);
            String shape = lineParts.get(7);
            Boolean canMega = Boolean.valueOf(lineParts.get(8));
            Boolean canGigantamax = Boolean.valueOf(lineParts.get(9));
            String eggGroup = lineParts.get(10);

           // pokedex.add(new Pokemon(nationalDexNumber, species, canEvolve, hasEvolved, pokemonType1, pokemonType2, color, shape, canMega, canGigantamax, eggGroup));
        }
    }
    
}
